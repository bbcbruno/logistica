package br.com.walmart.logistica.controller;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.walmart.logistica.banco.Mapa;
import br.com.walmart.logistica.banco.Rota;
import br.com.walmart.logistica.config.Application;
import br.com.walmart.logistica.rest.InfoCaminho;
import br.com.walmart.logistica.rest.Itinerario;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class MapasControllerTest {

	@Value("${local.server.port}")
	private int port;

	@Before
	public void setUp() {
		RestAssured.port = port;
	}

	@Test
	public void deveCadastrarUmMapa() {
		Response response = given().contentType("application/json")
				.body(new Mapa("SP")).post("/mapas");

		Mapa mapa = response.jsonPath().getObject("mapa", Mapa.class);
		assertThat(mapa, is(notNullValue()));
		assertThat(mapa.getId(), is(notNullValue()));
		assertThat(response.getStatusCode(), is(equalTo(HttpStatus.SC_CREATED)));
		response.then().body("_links.cadastrarRota.href", is(equalTo("http://localhost:" + port + "/mapas/1/rotas")));
	}

	@Test
	public void deveObterRota() {
		
		Mapa mapa = given().contentType("application/json")
				.body(new Mapa("SP")).post("/mapas")
				.jsonPath().getObject("mapa", Mapa.class);
		
		criarRota(mapa, "A", "B", 10L);
		criarRota(mapa, "B", "D", 15L);
		criarRota(mapa, "A", "C", 20L);
		criarRota(mapa, "C", "D", 30L);
		criarRota(mapa, "B", "E", 50L);
		criarRota(mapa, "D", "E", 30L);
		
		InfoCaminho info = new InfoCaminho("A", "D", new BigDecimal("10"), new BigDecimal("2.5"));
		
		Response response = given().contentType("application/json")
				.body(info).post("/mapas/{id}/obter-rota", mapa.getId());
		
		Itinerario itinerario = response.jsonPath().getObject("", Itinerario.class);
		
		assertThat(itinerario, is(notNullValue()));
		assertThat(itinerario.getCusto(), is(equalTo(new BigDecimal("6.25"))));
		assertThat(itinerario.getDistancia(), is(equalTo(25L)));
		assertThat(itinerario.getCaminho(), is(equalTo("A, B, D")));
		assertThat(response.getStatusCode(), is(equalTo(HttpStatus.SC_OK)));
		
	}
	
	private void criarRota(Mapa mapa, String origem, String destino, Long custo) {
		given().contentType("application/json")
				.body(new Rota(origem, destino, custo, null))
				.post("/mapas/{id}/rotas", mapa.getId());
	}

}
