package br.com.walmart.logistica.controller;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.walmart.logistica.banco.Mapa;
import br.com.walmart.logistica.banco.Rota;
import br.com.walmart.logistica.config.Application;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class RotasControllerTest {

	@Value("${local.server.port}")
	private int port;

	@Before
	public void setUp() {
		RestAssured.port = port;
	}

	@Test
	public void deveCadastrarUmaRota() {

		Mapa mapa = criarMapa();

		Response response = given().contentType("application/json")
				.body(new Rota("A", "B", 20L, null))
				.post("/mapas/{id}/rotas", mapa.getId());

		Rota rota = response.jsonPath().getObject("rota", Rota.class);
		assertThat(rota, is(notNullValue()));
		assertThat(rota.getId(), is(notNullValue()));
		assertThat(response.getStatusCode(), is(equalTo(HttpStatus.SC_CREATED)));
		String url = String.format("http://localhost:%d/mapas/%d/obter-rota", port, mapa.getId());
		response.then().body("_links.obterRota.href", is(equalTo(url)));
	}

	private Mapa criarMapa() {
		return given().contentType("application/json").body(new Mapa("SP"))
				.post("/mapas").jsonPath().getObject("mapa", Mapa.class);
	}

}
