package br.com.walmart.logistica.service;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.walmart.logistica.banco.Mapa;
import br.com.walmart.logistica.banco.Repository;
import br.com.walmart.logistica.banco.Rota;

public class RotaServiceTest {
	
	@InjectMocks
	private RotaServiceImpl service;
	
	@Mock
	private Repository repository;
	
	@Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }
	
	@Test(expected = IllegalArgumentException.class)
	public void deveLancarExcecaoSeMapaNaoExistir() {
		when(repository.buscar(Mapa.class, 1L)).thenReturn(null);
		Mapa mapa = new Mapa();
		mapa.setId(1L);
		service.cadastrar(new Rota("A", "B", 10L, mapa));
	}
	
}
