package br.com.walmart.logistica.rest;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;
import org.junit.Test;

import br.com.walmart.logistica.dijkstra.Algoritmo;
import br.com.walmart.logistica.dijkstra.Vertice;

public class ItinerarioTest {
	
	@Test
	public void devePreencherCorretamenteOItinerario() {
		
		Vertice verticeA = new Vertice("A");
		Vertice verticeB = new Vertice("B");
		Vertice verticeC = new Vertice("C");
		Vertice verticeD = new Vertice("D");
		verticeA.incluirAresta(verticeB, 2L);
		verticeB.incluirAresta(verticeA, 2L);
		verticeA.incluirAresta(verticeC, 6L);
		verticeC.incluirAresta(verticeA, 6L);
		verticeC.incluirAresta(verticeD, 4L);
		verticeD.incluirAresta(verticeC, 4L);
		Algoritmo dijkstra = new Algoritmo();
		dijkstra.calcularCaminho(verticeA);
		
		
		InfoCaminho infoCaminho =  new InfoCaminho("A", "D", new BigDecimal("10"), new BigDecimal("2.5"));
		Itinerario itinerario = new Itinerario(verticeD, infoCaminho);
		
		assertThat(itinerario.getCusto(), is(equalTo(new BigDecimal("2.5"))));
		assertThat(itinerario.getDistancia(), is(equalTo(10L)));
		assertThat(itinerario.getCaminho(), is(equalTo("A, C, D")));
		
	}

}
