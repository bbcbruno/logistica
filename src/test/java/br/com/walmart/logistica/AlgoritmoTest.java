package br.com.walmart.logistica;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import br.com.walmart.logistica.banco.Rota;
import br.com.walmart.logistica.dijkstra.Algoritmo;
import br.com.walmart.logistica.dijkstra.Vertice;

public class AlgoritmoTest {
	
	@Test
	public void deveEncontrarCaminhoEmGrafoComSomenteUmaAresta() {
		//  A - B
		Vertice verticeA = new Vertice("A");
		Vertice verticeB = new Vertice("B");
		verticeA.incluirAresta(verticeB, 8L);
		verticeB.incluirAresta(verticeA, 8L);
		Algoritmo dijkstra = new Algoritmo();
		dijkstra.calcularCaminho(verticeA);
		assertThat(verticeB.anterior().nome(), is(equalTo(verticeA.nome())));
	}
	
	@Test
	public void deveEncontrarMelhorCaminhoEmGrafoComArestaSolitaria() {
		//    B
		//   /
		//  A    D
		//   \  /
		//    C 
		Vertice verticeA = new Vertice("A");
		Vertice verticeB = new Vertice("B");
		Vertice verticeC = new Vertice("C");
		Vertice verticeD = new Vertice("D");
		verticeA.incluirAresta(verticeB, 2L);
		verticeB.incluirAresta(verticeA, 2L);
		verticeA.incluirAresta(verticeC, 6L);
		verticeC.incluirAresta(verticeA, 6L);
		verticeC.incluirAresta(verticeD, 4L);
		verticeD.incluirAresta(verticeC, 4L);
		Algoritmo dijkstra = new Algoritmo();
		dijkstra.calcularCaminho(verticeA);
		assertThat(verticeD.anterior().nome(), is(equalTo(verticeC.nome())));
		assertThat(verticeD.anterior().anterior().nome(), is(equalTo(verticeA.nome())));
	}
	
	@Test
	public void deveEncontrarMelhorCaminhoNoGrafoDeExemplo() {
		
		Vertice verticeA = new Vertice("A");
		Vertice verticeB = new Vertice("B");
		Vertice verticeC = new Vertice("C");
		Vertice verticeD = new Vertice("D");
		Vertice verticeE = new Vertice("E");
		
		verticeA.incluirAresta(verticeB, 10L); 
		verticeB.incluirAresta(verticeA, 10L); 
		verticeB.incluirAresta(verticeD, 15L); 
		verticeD.incluirAresta(verticeB, 15L); 
		verticeA.incluirAresta(verticeC, 20L); 
		verticeC.incluirAresta(verticeA, 20L); 
		verticeC.incluirAresta(verticeD, 30L); 
		verticeD.incluirAresta(verticeC, 30L); 
		verticeB.incluirAresta(verticeE, 50L); 
		verticeE.incluirAresta(verticeB, 50L); 
		verticeD.incluirAresta(verticeE, 30L); 
		verticeE.incluirAresta(verticeD, 30L); 
		
		Algoritmo dijkstra = new Algoritmo();
		dijkstra.calcularCaminho(verticeA);
		assertThat(verticeD.anterior().nome(), is(equalTo(verticeB.nome())));
		assertThat(verticeD.anterior().anterior().nome(), is(equalTo(verticeA.nome())));
	}
	
	@Test
	public void deveCriarVerticesCorretamente() {
		Algoritmo dijkstra = new Algoritmo();
		List<Rota> rotas = Arrays.asList(new Rota("A", "B", 5L, null), new Rota("A", "C", 4L, null));
		List<Vertice> vertices = dijkstra.criarVertices(rotas);
		assertThat(vertices.size(), is(equalTo(3)));
		assertThat(vertices.get(0).vizinhos().size(), is(equalTo(2)));
	}
	
	

}
