package br.com.walmart.logistica.banco;

public interface Repository {
	
	void cadastrar(Object o);
	<T> T buscar(Class<T> tipo, Long id);

}
