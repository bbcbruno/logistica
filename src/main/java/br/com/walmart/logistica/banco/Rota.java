package br.com.walmart.logistica.banco;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Rota {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String origem;
	
	private String destino;
	
	private Long custo;
	
	public Rota() {	}
	
	public Rota(String origem, String destino, Long custo, Mapa mapa) {
		this.origem = origem;
		this.destino = destino;
		this.custo = custo;
		this.mapa = mapa;
	}

	@ManyToOne
	@JoinColumn(name = "mapa_id")
	private Mapa mapa;
	
	public String getDestino() {
		return destino;
	}
	
	public String getOrigem() {
		return origem;
	}
	
	public Long getCusto() {
		return custo;
	}
	
	public Mapa getMapa() {
		return mapa;
	}
	
	public Long getId() {
		return id;
	}

	public void setMapa(Mapa mapa) {
		this.mapa = mapa;
	}
	
	public void setCusto(Long custo) {
		this.custo = custo;
	}
	
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	
}
