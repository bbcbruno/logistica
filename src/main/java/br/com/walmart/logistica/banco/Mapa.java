package br.com.walmart.logistica.banco;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Mapa {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String nome;
	
	@OneToMany(mappedBy="mapa")
	private List<Rota> rotas;
	
	public Mapa() { }
	
	public Mapa(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	@JsonIgnore
	public List<Rota> getRotas() {
		return rotas;
	}

}
