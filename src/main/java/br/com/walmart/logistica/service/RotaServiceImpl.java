package br.com.walmart.logistica.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.walmart.logistica.banco.Mapa;
import br.com.walmart.logistica.banco.Repository;
import br.com.walmart.logistica.banco.Rota;

@Service
public class RotaServiceImpl implements RotaService {

	@Autowired
	private Repository repository;
	
	@Override
	public void cadastrar(Rota rota) {
		Mapa mapa = repository.buscar(Mapa.class, rota.getMapa().getId());
		if (mapa == null) throw new IllegalArgumentException("Id do mapa inexistente");
		rota.setMapa(mapa);
		repository.cadastrar(rota);
	}

}
