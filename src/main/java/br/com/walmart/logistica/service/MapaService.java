package br.com.walmart.logistica.service;

import br.com.walmart.logistica.banco.Mapa;
import br.com.walmart.logistica.rest.InfoCaminho;
import br.com.walmart.logistica.rest.Itinerario;

public interface MapaService {
	
	void cadastrar(Mapa mapa);
	Itinerario obterItinerario(Mapa mapa, InfoCaminho infoCaminho);

}
