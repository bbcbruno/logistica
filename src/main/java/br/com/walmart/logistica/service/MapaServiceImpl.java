package br.com.walmart.logistica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.walmart.logistica.banco.Mapa;
import br.com.walmart.logistica.banco.Repository;
import br.com.walmart.logistica.dijkstra.Algoritmo;
import br.com.walmart.logistica.dijkstra.Vertice;
import br.com.walmart.logistica.rest.InfoCaminho;
import br.com.walmart.logistica.rest.Itinerario;

@Service
public class MapaServiceImpl implements MapaService {

	@Autowired
	private Repository repository;
	
	@Autowired
	private Algoritmo algoritmo;
	
	@Override
	public void cadastrar(Mapa mapa) {
		repository.cadastrar(mapa);
	}

	@Override
	public Itinerario obterItinerario(Mapa mapa, InfoCaminho infoCaminho) {
		mapa = repository.buscar(Mapa.class, mapa.getId());
		List<Vertice> vertices = algoritmo.criarVertices(mapa.getRotas());
		
		Vertice origem = vertices
				.stream()
				.filter(vertice -> vertice.nome().equals(infoCaminho.getOrigem()))
				.findFirst()
				.get();
		
		Vertice destino = vertices
				.stream()
				.filter(vertice -> vertice.nome().equals(infoCaminho.getDestino()))
				.findFirst()
				.get();
		
		algoritmo.calcularCaminho(origem);
		return new Itinerario(destino, infoCaminho);
		
	}

}
