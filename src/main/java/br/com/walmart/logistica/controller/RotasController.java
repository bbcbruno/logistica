package br.com.walmart.logistica.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.walmart.logistica.banco.Mapa;
import br.com.walmart.logistica.banco.Rota;
import br.com.walmart.logistica.service.RotaService;


@RestController
@RequestMapping("/mapas/{id}/rotas")
public class RotasController {
	
	@Autowired
	private RotaService service;
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<br.com.walmart.logistica.rest.Rota> cadastrar(@RequestBody Rota rota, Mapa mapa) {
		rota.setMapa(mapa);
		service.cadastrar(rota);
		br.com.walmart.logistica.rest.Rota recurso = new br.com.walmart.logistica.rest.Rota(rota);
		Link link = linkTo(methodOn(MapasController.class, rota.getMapa().getId()).calcularRota(rota.getMapa(), null)).withRel("obterRota");
		recurso.add(link);
		return new ResponseEntity<br.com.walmart.logistica.rest.Rota>(recurso, HttpStatus.CREATED);
	}

}
