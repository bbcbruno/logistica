package br.com.walmart.logistica.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.walmart.logistica.banco.Mapa;
import br.com.walmart.logistica.rest.InfoCaminho;
import br.com.walmart.logistica.rest.Itinerario;
import br.com.walmart.logistica.service.MapaService;

@RestController
@RequestMapping("/mapas")
public class MapasController {
	
	@Autowired
	private MapaService service;
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<br.com.walmart.logistica.rest.Mapa> cadastrar(@RequestBody Mapa mapa) {
		service.cadastrar(mapa);
		br.com.walmart.logistica.rest.Mapa recurso = new br.com.walmart.logistica.rest.Mapa(mapa); 
		Link link = linkTo(RotasController.class, mapa.getId()).withRel("cadastrarRota");
		recurso.add(link);
		return new ResponseEntity<br.com.walmart.logistica.rest.Mapa>(recurso, HttpStatus.CREATED);
		
	}
	
	@RequestMapping(value="/{id}/obter-rota", method=RequestMethod.POST)
	public Itinerario calcularRota(Mapa mapa, @RequestBody InfoCaminho infoCaminho) {
		return service.obterItinerario(mapa, infoCaminho);
	}

}
