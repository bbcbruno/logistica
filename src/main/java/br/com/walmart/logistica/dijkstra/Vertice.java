package br.com.walmart.logistica.dijkstra;

import java.util.ArrayList;
import java.util.List;

public class Vertice {
    
	private String nome;
    
	private List<Aresta> vizinhos;
    
    private long distancia = Long.MAX_VALUE;
    
    private Vertice anterior;
    
    public Vertice (String nome) { 
    	this.nome = nome; 
    	this.vizinhos = new ArrayList<>();
    }

    public void incluirAresta(Vertice vertice, Long distancia) {
		vizinhos.add(new Aresta(vertice, distancia));
	}
    
    public void setDistancia(long distancia) {
		this.distancia = distancia;
	}
    
    public List<Aresta> vizinhos() {
		return vizinhos;
	}
    
    public long distancia() {
		return distancia;
	}
    
    public void incluirAnterior(Vertice anterior) {
    	this.anterior = anterior;
    }
    
    public Vertice anterior() {
		return anterior;
	}
    
    public String nome() {
		return nome;
	}
    
}
