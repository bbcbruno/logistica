package br.com.walmart.logistica.dijkstra;

public class Aresta {
    
	private Vertice destino;
    private Long custo;
    
    public Aresta(Vertice destino, Long custo) {
    	this.destino = destino;
    	this.custo = custo;
    }
    
    public Long getCusto() {
		return custo;
	}
    
    public Vertice getDestino() {
		return destino;
	}
}
