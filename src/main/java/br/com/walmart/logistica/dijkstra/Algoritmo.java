package br.com.walmart.logistica.dijkstra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.function.Consumer;

import org.springframework.stereotype.Component;

import br.com.walmart.logistica.banco.Rota;

@Component
public class Algoritmo {
	
	public List<Vertice> criarVertices(List<Rota> rotas) {
		
		Map<String, Vertice> vertices = new HashMap<>();
		
		rotas.forEach(new Consumer<Rota>() {

			public void accept(Rota rota) {
				Vertice origem = vertices.get(rota.getOrigem());
				if(origem == null) {
					origem =  new Vertice(rota.getOrigem());
					vertices.put(rota.getOrigem(), origem);
				}
				
				Vertice destino = vertices.get(rota.getDestino());
				if(destino == null) {
					destino =  new Vertice(rota.getDestino());
					vertices.put(rota.getDestino(), destino);
				}
				
				origem.incluirAresta(destino, rota.getCusto());
				destino.incluirAresta(origem, rota.getCusto());
			
			}
		});
		
		return new ArrayList<Vertice>(vertices.values());
	}
    
	public void calcularCaminho(Vertice origem) {
		
		origem.setDistancia(0);
        PriorityQueue<Vertice> verticesAbertos = new PriorityQueue<Vertice>((a,b) -> Long.compare(a.distancia(), b.distancia()));
        verticesAbertos.add(origem);

        while (!verticesAbertos.isEmpty()) {
        	Vertice atual = verticesAbertos.poll();

            for (Aresta aresta : atual.vizinhos()) {
            	Vertice destino = aresta.getDestino();
                
                long distanciaParaDestino = atual.distancia() + aresta.getCusto();
		        
                if (distanciaParaDestino < destino.distancia()) {
		            verticesAbertos.remove(atual);
		            destino.setDistancia(distanciaParaDestino);
		            destino.incluirAnterior(atual);
		            verticesAbertos.add(destino);
		        }
            }
        }
    }

}
