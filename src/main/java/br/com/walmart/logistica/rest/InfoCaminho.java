package br.com.walmart.logistica.rest;

import java.math.BigDecimal;

public class InfoCaminho {
	
private String origem;
	
	private String destino;
	
	private BigDecimal autonomia;
	
	private BigDecimal valorLitro;
	
	public InfoCaminho() { }
	
	public InfoCaminho(String origem, String destino, BigDecimal autonomia, BigDecimal valorLitro) {
		this.origem = origem;
		this.destino = destino;
		this.autonomia = autonomia;
		this.valorLitro = valorLitro;
	}

	public BigDecimal getAutonomia() {
		return autonomia;
	}
	
	public String getDestino() {
		return destino;
	}
	
	public String getOrigem() {
		return origem;
	}
	
	public BigDecimal getValorLitro() {
		return valorLitro;
	}


}
