package br.com.walmart.logistica.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import br.com.walmart.logistica.dijkstra.Vertice;

public class Itinerario {
	
	private String caminho;
	
	private Long distancia;
	
	private BigDecimal custo;
	
	public Itinerario() { }
	
	public Itinerario(Vertice destino, InfoCaminho info) {
		List<Vertice> vertices = extrairVertices(destino);
		preencherCaminho(vertices);
		distancia = destino.distancia();
		custo = BigDecimal.valueOf(distancia).divide(info.getAutonomia()).multiply(info.getValorLitro());
	}

	private List<Vertice> extrairVertices(Vertice destino) {
		List<Vertice> vertices = new ArrayList<>();
		vertices.add(destino);
		while(destino.anterior() != null) {
			vertices.add(destino.anterior());
			destino = destino.anterior();
		}
		return vertices;
	}

	private void preencherCaminho(List<Vertice> vertices) {
		List<String> nomes = vertices.stream().map(Vertice::nome).collect(Collectors.toList());
		Collections.reverse(nomes);
		caminho = String.join(", ", nomes);
	}
	
	public String getCaminho() {
		return caminho;
	}
	
	public BigDecimal getCusto() {
		return custo;
	}
	
	public Long getDistancia() {
		return distancia;
	}
	
}
