package br.com.walmart.logistica.rest;

import org.springframework.hateoas.ResourceSupport;

public class Mapa extends ResourceSupport {
	
	private br.com.walmart.logistica.banco.Mapa mapa;
	
	public Mapa(br.com.walmart.logistica.banco.Mapa mapa) {
		this.mapa = mapa;
	}
	
	public br.com.walmart.logistica.banco.Mapa getMapa() {
		return mapa;
	}

}
