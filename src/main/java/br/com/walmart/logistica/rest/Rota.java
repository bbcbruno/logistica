package br.com.walmart.logistica.rest;

import org.springframework.hateoas.ResourceSupport;

public class Rota extends ResourceSupport {
	
	private br.com.walmart.logistica.banco.Rota rota;
	
	public Rota(br.com.walmart.logistica.banco.Rota rota) {
		this.rota = rota;
	}
	
	public br.com.walmart.logistica.banco.Rota getRota() {
		return rota;
	}

}
