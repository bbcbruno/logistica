# INFORMAÇÕES #

As tecnologias utilizadas nesse projeto são:

* Java 8
* Spring Boot
* Rest Assured
* HSQLDB

Foi criada uma api REST e abaixo segue alguns exemplos de requests.


Post: http://localhost:8080/mapas

{
  "nome": "SP"
}

Post: http://localhost:8080/mapas/1/rotas

{
  "origem": "A",
  "destino": "C",
  "custo": 20
}

Post: http://localhost:8080/mapas/1/obter-rota

{
  "origem": "A",
  "destino": "G",
  "autonomia": "10",
  "valorLitro": "2.5"
}
